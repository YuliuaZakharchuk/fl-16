// Your code goes here
const birthdayModule = require('../src/birthday.service');

describe("Birthday Service", function() {
    it("birthday is today", function() {
        let result = new birthdayModule.BirthdayService(new Date()).howLongForMyBirthday();
        expect(result).toBe('Hooray!!! It is today!');
    });
    it("wait for birthday ", function() {
        let result = new birthdayModule.BirthdayService(new Date()).notifyWaitingTime();
        expect(result).toBe(`Soon...Please, wait just undefined day/days`);
    });
    it("soon  birthday ", function() {
        let result = new birthdayModule.BirthdayService(new Date()).notifyWaitingTimes();
        expect(result).toBe(`Oh, you have celebrated it NaN day/s ago, dont you remember?`);
    });

});