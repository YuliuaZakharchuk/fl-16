// Your code goes here
// describe("Element is number", function(arguments) {
//     it("should be a number", function(arguments) {
//         expect(arguments).toBe(typeof number);
//     });
// });
const biggestNumber = require('../src/get-bigest-number');

describe("Biggest Number function", function() {
    it("returns the biggest number", function() {
        let result = biggestNumber.getBiggestNumber(1, 2, 4, 3);
        expect(result).toBe(4);
    });
    it("failed on wrong argument type", function() {
        expect(function() {
            biggestNumber.getBiggestNumber('Hello');
        }).toThrow(new Error("Wrong argument type"));
    });
    it("failed on not enough arguments", function() {
        expect(function() {
            biggestNumber.getBiggestNumber(1);
        }).toThrow(new Error("Not enough arguments"));
    });
    it("failed to many arguments", function() {
        expect(function() {
            biggestNumber.getBiggestNumber(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
        }).toThrow(new Error("Too many arguments"));
    });
    it("failed argument type is not a number", function() {
        expect(function() {
            biggestNumber.getBiggestNumber(1, 2, 3, 'a', 5, 6, 7);
        }).toThrow(new Error("Wrong argument type"));
    });
});