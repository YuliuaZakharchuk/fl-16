// Your code goes here

function getBiggestNumber(...argument) {
    for (let i = 0; i < argument.length; i++) {
        if (typeof argument[i] !== 'number') {
            throw new Error('Wrong argument type');
        }
    }
    let num = 2;
    if (argument.length < num) {
        throw new Error('Not enough arguments');
    }
    let arg = 10;
    if (argument.length > arg) {
        throw new Error('Too many arguments');
    }
    let biggestNumber = Math.max.apply(null, argument);
    return biggestNumber;
}

module.exports = {
    getBiggestNumber
};