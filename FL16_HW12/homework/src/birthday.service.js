// Your code goes here
class BirthdayService {
    constructor(birthdayDate) {
        this.birthdayDate = birthdayDate;

    }

    howLongForMyBirthday() {
        let today = new Date(),
            nextBirthday = new Date(today.getFullYear(), this.birthdayDate.getMonth(), this.birthdayDate.getUTCDate());
        today.setHours(0, 0, 0, 0);
        if (today > nextBirthday) {
            nextBirthday.setFullYear(today.getFullYear() + 1);
        }
        let current = 8.64e7
        let daysToBirthday = Math.round((nextBirthday - today) / current);
        if (daysToBirthday === 0) {
            return this.congratulateWithBirthday();
        }
        let num = 183;
        if (daysToBirthday < num) {
            return this.notifyWaitingTime(daysToBirthday);
        }
        if (daysToBirthday > num) {
            return this.notifyWaitingTimes(daysToBirthday);
        }
        return daysToBirthday;
    }
    congratulateWithBirthday() {
        return 'Hooray!!! It is today!';
    }
    notifyWaitingTime(daysToBirthday) {
        return `Soon...Please, wait just ${daysToBirthday} day/days`;
    }
    notifyWaitingTimes(daysToBirthday) {
        let now = 365;
        return `Oh, you have celebrated it ${now-daysToBirthday} day/s ago, dont you remember?`;
    }


}
module.exports = {
    BirthdayService
};