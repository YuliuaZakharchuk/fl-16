class PizzaException {
    constructor(err) {
        this.error = err;
    }
    log() {
        console.log(`${this.error}`)
    }
}

class Pizza {
    static SIZE_S = 50;
    static SIZE_M = 75;
    static SIZE_L = 100;
    static ALLOWED_SIZES = [this.SIZE_S, this.SIZE_M, this.SIZE_L];

    static EXTRA_CHEESE = 7;
    static EXTRA_MEAT = 9;
    static EXTRA_TOMATOES = 5;
    static ALLOWED_EXTRAS = [this.EXTRA_CHEESE, this.EXTRA_MEAT, this.EXTRA_TOMATOES];

    static TYPE_VEGGIE = 50;
    static TYPE_MARGHERITA = 60;
    static TYPE_PEPPERONI = 70;
    static ALLOWED_TYPES = [this.TYPE_VEGGIE, this.TYPE_MARGHERITA, this.TYPE_PEPPERONI];

    constructor(size, type) {
        this.size = size;
        this.type = type;
        this.extra = [];

        if (!Pizza.ALLOWED_SIZES.includes(size)) {
            throw new PizzaException('Invalid size')
        }
        if (!Pizza.ALLOWED_TYPES.includes(type)) {
            throw new PizzaException('Invalid type')
        }
    }
    addExtraIngredient(ingredient) {
        if (!Pizza.ALLOWED_EXTRAS.includes(ingredient)) {
            throw new PizzaException('Invalid ingredient')
        }
        if (this.extra.includes(ingredient)) {
            throw new PizzaException('Duplicate ingredient')
        }
        this.extra.push(ingredient);
        return this.extra;
    }
    getExtraIngredient() {
        return this.extra;

    }
    getSize() {
        return this.size;
    }
    getType() {
        return this.type;
    }
    removeExtraIngredient(ingredient) {
        const index = this.extra.indexOf(ingredient);
        if (index > -1) {
            this.extra.splice(index, 1);
        };
        return this.extra;

    }
    getPizzaInfo() {
        return `Size: ${this.size}, type: ${this.type}, extra ingredients: ${this.extra}, price: ${this.getPrice()}`;

    }

    getPrice() {
        let extra_price = 0;
        for (let i = 0; i < this.extra.length; i++) {
            extra_price += this.extra[i];
        }
        return this.size + this.type + extra_price;
    }
}

let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_PEPPERONI);
let pizza2 = new Pizza(Pizza.SIZE_L, Pizza.TYPE_MARGHERITA);
let pizza3 = new Pizza(Pizza.SIZE_M, Pizza.TYPE_VEGGIE);
console.log(`Price(${pizza.size}, ${pizza.type}): ${pizza.getPrice()} UAH`);
console.log(`Price(${pizza2.size}, ${pizza2.type}): ${pizza2.getPrice()} UAH`);
console.log(`Price(${pizza3.size}, ${pizza3.type}): ${pizza3.getPrice()} UAH`);


pizza.addExtraIngredient(Pizza.EXTRA_MEAT)
pizza.addExtraIngredient(Pizza.EXTRA_CHEESE)
console.log(`Price with ingredients: ${pizza.getPrice()} UAH`);
console.log(`Ingredient: ${pizza.getExtraIngredient()}`)
pizza.removeExtraIngredient(Pizza.EXTRA_CHEESE);
console.log(`Ingredient: ${pizza.getExtraIngredient()}`)
console.log(pizza.getPizzaInfo()); //=> Size: SMALL, type: VEGGIE; extra ingredients: MEAT,TOMATOES; price: 114UAH.

pizza = new Pizza(Pizza.SIZE_S, Pizza.SIZE_S);
console.log(pizza.addExtraIngredient())

/* It should work */
// small pizza, type: veggie
// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// add extra meat
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// check price
// console.log(`Price: ${pizza.getPrice()} UAH`); //=> Price: 109 UAH
// add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_CHEESE);
// add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_TOMATOES);
// check price
// console.log(`Price with extra ingredients: ${pizza.getPrice()} UAH`); // Price: 121 UAH
// check pizza size
// console.log(`Is pizza large: ${pizza.getSize() === Pizza.SIZE_L}`); //=> Is pizza large: false
// remove extra ingredient
// pizza.removeExtraIngredient(Pizza.EXTRA_CHEESE);
// console.log(`Extra ingredients: ${pizza.getExtraIngredients().length}`); //=> Extra ingredients: 2
// console.log(pizza.getPizzaInfo()); //=> Size: SMALL, type: VEGGIE; extra ingredients: MEAT,TOMATOES; price: 114UAH.


// examples of errors
// pizza = new Pizza(Pizza.SIZE_S); // => Required two arguments, given: 1

pizza = new Pizza(Pizza.SIZE_S, Pizza.SIZE_S); // => Invalid type

// pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Duplicate ingredient

// pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Invalid ingredient