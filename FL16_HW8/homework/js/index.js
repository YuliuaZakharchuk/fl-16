// Write your code here
function insrt(num) {
    $('.calc-display').val($('.calc-display').val() + num);
}

let rows = 0;

function addRow(text) {
    rows++;
    let rowClass = `row-${rows}`;
    $('.history').append(`<div class="${rowClass}"></div>`);
    let row = $(`.${rowClass}`);
    row.append('<span class="circle"></span>');
    row.append(`<p>${text}</p>`);
    row.append('<p style="color:red;">X</p>');
}

function evil(fn) {
    return new Function('return ' + fn)();
}

function calculate() {
    let value_before = $('.calc-display').val();
    let value_after = evil(value_before);
    if (value_after === Infinity) {
        $('.calc-display').val(`Error`);
        $('.result').val('');
    } else {
        $('.calc-display').val(value_after);
        $('.result').val(value_before + "=" + value_after);
        addRow(value_before + "=" + value_after);
    }
}

function c() {
    $('.calc-display').val('');
}