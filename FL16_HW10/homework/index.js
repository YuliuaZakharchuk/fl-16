//write your code here
class Magazine {
    constructor(name, article, magazine) {
        this.name = name;
        this.article = article;
        this.magazine = magazine;
    }
    getInfo() {
        return `${this.name},${this.article}`
    }

}

// create staff
class MagazineEmployee extends Magazine {
    // constructor(name, article, magazine) {
    //     super(name, article, magazine)
    // }
    getName() {
        return this.name;
    }
    getArticle() {
        return this.article;
    }
}

const magazine = new Magazine();
const manager = new MagazineEmployee('Andrii', 'manager', magazine);
const sport = new MagazineEmployee('Serhii', 'sport', magazine);
const politics = new MagazineEmployee('Volodymyr', 'politics', magazine);
const general = new MagazineEmployee('Olha', 'general', magazine);

console.log(manager.getName());
console.log(manager.getArticle());
console.log(sport.getName());
console.log(sport.getArticle());
console.log(politics.getName());
console.log(politics.getArticle());
console.log(general.getName());
console.log(general.getArticle());
console.log(manager.getInfo());



//create followers
class Observer {
    constructor(state) {
        this.state = state;
        this.initialState = state;
    }
    update(action) {
        switch (action.type) {
            case 'INCREMENT':
                this.state = ++this.state;
                break;
            case 'DECREMENT':
                this.state = --this.state;
                break;
            default:
                this.state = this.initialState;
        }
    }
}
class Follower {
    constructor() {
        this.observers = [];
    }
    onUpdate(data) {
        return data;
    }
    subscribeTo(observer) {
        this.observers.push(observer);
    }
    unsubscribeTo(observer) {
        this.observers = this.observers.filter(obs => obs !== observer);
    }
    fire(action) {
        this.observers.forEach(observer => {
            observer.update(action);

        })
    }
}


const stream$ = new Follower();
const obs1 = new Observer(1);
const obs2 = new Observer(42);
const iryna = new Follower('Iryna');
console.log(Follower.observe);
const maksym = new Follower('Maksym');
console.log(maksym);
const mariya = new Follower('Mariya');
console.log(mariya)
const olya = new Follower('Olya');
const obs3 = new Observer('sport');
olya.subscribeTo(obs3);
console.log(obs3.state);
olya.subscribeTo(obs1);
stream$.subscribeTo(obs2);
console.log(obs1.state);
console.log(obs2.state);
// console.log(iryna.subscribeTo(magazine, 'sport'));
// console.log(maksym.subscribeTo(magazine, 'politics'));
// console.log(mariya.subscribeTo(magazine, 'politics'));
// console.log(mariya.subscribeTo(magazine, 'general'));




//class TOPIC 
class TypeNews {
    constructor(type) {
        this.type = type;
    }
    addArticle(b) {
        return b + ` iryna`;
    }
}
const info = new TypeNews();
console.log(info.addArticle('something wrong'))




// class change status 
class OrderStatus {
    constructor(name, nextStatus) {
        this.name = name;
        this.nextStatus = nextStatus;
    }
    next() {
        return new this.NextStatus();
    }
}

//states 

class ReadyForPushNotification extends OrderStatus {
    constructor() {
        super('readyForPushNotification', ReadyForApprove);
    }
    publish() {
        return `Hello ${name}. You can't publish. We are creating
        publications now.`;
    }
    approve() {
        return `Hello ${name}. You can't approve. We don't have enough
        of publications.`
    }
}

class ReadyForApprove extends OrderStatus {
    constructor() {
        super('readyForApprove', ReadyForPublish);
    }
    publish() {
        return `Hello ${name} You can't publish. We don't have a manager's approval.`;
    }
    approve() {
        return `Hello ${name} You've approved the changes`
    }
}

class ReadyForPublish extends OrderStatus {
    constructor() {
        super('readyForPublish', PublishInProgress);
    }
    publish() {
        return `Hello ${name} You've recently published
        publications.`;
    }
    approve() {
        return `Hello ${name} Publications have been already approved by
        you.`
    }
}
class PublishInProgress extends OrderStatus {
    constructor() {
        super('publishInProgress', PublishInProgress);
    }
    publish() {
        return `Hello ${name}. While we are publishing we can't do any actions.`;
    }
    approve() {
        return `Hello ${name}. While we are publishing we can't do any actions`;
    }
}

//order
class Order {
    constructor() {
        this.state = new ReadyForPushNotification();
    }
    nextState() {
        return this.state.next();
    }
}

// create order
const myOrder = new Order();
console.log(myOrder.state.name);
myOrder.nextState();
console.log(myOrder.state.name);
myOrder.nextState();
console.log(myOrder.state.name);
myOrder.nextState();
console.log(myOrder.state.name);