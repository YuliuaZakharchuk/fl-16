const arr = ['1', '3', '4', '2', '5']

function getMaxEvenElement(arr) {
    let max = Math.max(...arr)
    return max;
}
console.log(getMaxEvenElement(arr))


let a = 3;
let b = 5;
[a, b] = [b, a]
console.log(a)


function getValue(v) {
    if (v || null) {
        return v;
    } else {
        return `-`;
    }
}
console.log(getValue('0'))
console.log(getValue('4'))
console.log(getValue('SomeText'))
console.log(getValue(null))
console.log(getValue(undefined))


const arrayOfArrays = [
    ['name', 'dan'],
    ['age', '21'],
    ['city', 'Lviv']
]

function getObjFromArray(arrayOfArrays) {
    return arrayOfArrays.reduce(function(result, currentArray) {
        result[currentArray[0]] = currentArray[1];
        return result;
    }, {});
}
console.log(getObjFromArray(arrayOfArrays))


const addUniqueId = obj => {
    let id = Symbol();
    return {...obj, id }
}
console.log(addUniqueId({ name: 123 }));
console.log(Object.keys({ name: 123 }).includes('id'))


const getRegroupedObject = obj => {
    const {
        name: firstName,
        details: { id, age, university }
    } = obj;
    const user = { age, firstName, id }
    return { university, user };
}
const oldObj = {
    name: "willow",
    details: { id: 1, age: 47, university: "LNU" }
}
console.log(getRegroupedObject(oldObj));


let duplicates = new Set([2, 3, 4, 2, 4, 'a', 'b', 'a']);
console.log(duplicates.values());


function padFunction(number) {
    let string = String(number)
    let sliced = string.slice(-4);
    let mask = String(sliced).padStart(string.length, "*")
    return mask;
}
console.log(padFunction('0123456789'))



function required(varName) {
    throw new Error(`${varName} is required. `);
}

function sumOfTwoValues(a = required("a"), b = required("b")) {
    return a + b
}

console.log(sumOfTwoValues(2, 3));
console.log(sumOfTwoValues(2));


const generatorObject = generateIterableSequence(['I', 'love', 'EPAM']);

function* generateIterableSequence(generatorObject) {
    for (let value of generatorObject) {
        yield value
        console.log(value)
    }
}
generatorObject.next()
generatorObject.next()
generatorObject.next()
generatorObject.next()