import '../scss/style.scss';
import '../scss/color.scss';
import './func.js';
import './index.html';


let buttonAddTweet = document.getElementById('navigationButtons');
let simpleTweet = document.getElementById('tweetItems');
let edit = document.getElementById('modifyItem');
let buttonGoToLiked = document.createElement('button');
buttonAddTweet.addEventListener('click', function() {
    edit.style.visibility = 'visible';
    simpleTweet.style.display = 'none';
    history.pushState(history.state, 'Edit Tweeter', '#/edit/:item_id addtwiter');
    let num = 140;
    if (edit === 0 || edit.length > num) {
        alert('Error! You can not tweet about that');
    }
})

let buttonSaveChanges = document.getElementById('saveModifiedItem');
buttonSaveChanges.addEventListener('click', function() {
    let list = document.getElementById('list');
    let p = document.createElement('p');
    let buttonRemove = document.createElement('button');
    let buttonLike = document.createElement('button');
    buttonRemove.innerHTML = 'Remove';
    buttonRemove.addEventListener('click', function() {
        edit.style.visibility = 'visible';
        simpleTweet.style.display = 'none';
        history.pushState(null, 'Edit Tweeter', '#/edit/:item_id addtwiter');
    })
    buttonLike.innerHTML = 'Like';
    buttonLike.addEventListener('click', function() {
        history.pushState(history.state, 'Liked Tweeters', '#/edit/:item_id liked');
        buttonGoToLiked.innerHTML = 'Go to liked';
        buttonLike.innerHTML = 'unliked'
        buttonLike.setAttribute('disabled', true);
        simpleTweet.appendChild(buttonGoToLiked);
    })
    let valueTextArea = document.getElementById('modifyItemInput').value;
    p.innerHTML = valueTextArea;

    list.appendChild(p);
    list.appendChild(buttonRemove);
    list.appendChild(buttonLike);
    simpleTweet.style.visibility = 'visible';
    simpleTweet.style.display = 'block';
    edit.style.visibility = 'hidden';
    history.pushState(history.state, 'Edit Tweeter', '#/edit/:item_id suffix');
})
buttonGoToLiked.addEventListener('click', function() {
    history.pushState(history.state, 'Liked Tweeters', '#/edit/:item_id liked');
    simpleTweet.style.display = 'none';

})