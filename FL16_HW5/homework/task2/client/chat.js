const messages = document.getElementById('messages');
const form = document.getElementById('form');
const input = document.getElementById('input');
const statuss = document.getElementById('statuss');
const ws = new WebSocket('ws://localhost:8080');

function setStatus(value) {
    statuss.innerHTML = value;
}

function printMessage(value) {
    const li = document.createElement('li');
    li.innerHTML = value;
    messages.appendChild(li);
}
form.addEventListener('submit', event => {
    event.preventDefault();
    ws.send(input.value);
    input.value = ''
})
ws.onopen = () =>
    setStatus('ONLINE');
ws.onclose = () => setStatus('DISCONECTED');
ws.onmessage = response => printMessage(response.data);