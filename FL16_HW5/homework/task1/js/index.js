// Your code goes here

const requestURL = 'https://jsonplaceholder.typicode.com/users';
const xhr = new XMLHttpRequest()
xhr.open('GET', requestURL)
xhr.responseType = 'json'
xhr.onload = () => {
    createInput()
    for (let i = 0; i < xhr.response.length; i++) {
        let tbody = document.getElementById('table').getElementsByTagName('tbody')[0];
        let newRow = tbody.insertRow();
        let newCell = newRow.insertCell();
        let newId = document.createTextNode(xhr.response[i].id);
        let newName = document.createTextNode(xhr.response[i].name);
        let newEmail = document.createTextNode(xhr.response[i].email);
        let newPhone = document.createTextNode(xhr.response[i].phone);
        newRow.id = `person-${xhr.response[i].id}`


        newCell.appendChild(newId);
        newCell.appendChild(newName);
        newCell.appendChild(newEmail);
        newCell.appendChild(newPhone);
        let btn_edit = document.createElement('button');
        btn_edit.innerHTML = 'Edit';
        newCell.appendChild(btn_edit);
        btn_edit.addEventListener('click', updateUserData)
        let btn_delete = document.createElement('button');
        btn_delete.innerHTML = 'Delete';
        newCell.appendChild(btn_delete);
        // btn_delete.addEventListener('click', deleteUser);
    }
}
xhr.send();

function createInput() {
    let inputId = document.createElement('input');
    inputId.id = "input-id"
    inputId.placeholder = 'new'
    document.body.appendChild(inputId);
    let inputName = document.createElement('input');
    inputName.innerHTML = 'new'
    document.body.appendChild(inputName);
    let inputEmail = document.createElement('input');
    inputEmail.innerHTML = 'new'
    document.body.appendChild(inputEmail);
    let inputPhone = document.createElement('input');
    inputPhone.innerHTML = 'new'
    document.body.appendChild(inputPhone);
}



function updateUserData(id, name, email, phone) {
    fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
            method: 'PUT',
            body: JSON.stringify({
                id,
                name,
                email,
                phone
            }),
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        })
        // .then(response => response.json())
        // .then(json => renderUsers(json))
        .catch(console.log)
        .then(showSpinner());
}



const spinner = document.getElementById("spinner");

function showSpinner() {
    spinner.className = "show";
    setTimeout(() => {
        spinner.className = spinner.className.replace("show", "");
    }, 5000);
}

// function EditOnClick() {}
// button update
// put server
// update 1 row