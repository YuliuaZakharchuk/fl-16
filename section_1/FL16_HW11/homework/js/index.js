function visitLink(path) {
    let count = localStorage.getItem(path);
    if (!count) {
        count = 0;
    }
    localStorage.setItem(path, 1 + parseInt(count, 10));
}

function viewResults() {
    //your code goes here
    alert(
        `You visited Page1 ${localStorage.getItem("Page1")} time(s)\n 
         You visited Page2 ${localStorage.getItem("Page2")} time(s)\n 
         You visited Page3 ${localStorage.getItem("Page3")} time(s)`);
    localStorage.clear();
}