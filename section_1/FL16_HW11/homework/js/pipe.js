function isFunction(functionToCheck) {
    return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

const pipe = (value, ...functions) => {
    // PIPE implementation
    let result = value;
    for (var i = 0; i < functions.length; i++) {
        let func = functions[i];
        if (!isFunction(func)) {
            return `Provided argument at position ${i} is not a function!`;
        }
        result = func(result);
    }
    return result;
};

const replaceUnderscoreWithSpace = (value) => value.replace(/_/g, ' ');
const capitalize = (value) =>
    value
    .split(' ')
    .map((val) => val.charAt(0).toUpperCase() + val.slice(1))
    .join(' ');
const appendGreeting = (value) => `Hello, ${value}!`;

const error = pipe('john_doe', replaceUnderscoreWithSpace, capitalize, '');
alert(error); // Provided argument at position 2 is not a function!
const result = pipe('john_doe', replaceUnderscoreWithSpace, capitalize, appendGreeting);
alert(result); // Hello, John Doe!