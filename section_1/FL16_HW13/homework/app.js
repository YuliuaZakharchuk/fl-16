const appRoot = document.getElementById('app-root');

/*
write your code here

list of all regions
externalService.getRegionsList();
list of all languages
externalService.getLanguagesList();
get countries list by language
externalService.getCountryListByLanguage()
get countries list by region
externalService.getCountryListByRegion()
*/


//Create and append select list
let myParentRegion = document.body;
let selectList = document.createElement("select");
myParentRegion.appendChild(selectList);


function removeOptions() {
    let i, L = selectList.length - 1;
    for (i = L; i >= 0; i--) {
        selectList.remove(i);
    }
}


function getRegionsList() {

    let array = externalService.getRegionsList();
    for (let i = 0; i < array.length; i++) {
        let option = document.createElement("option");
        option.value = array[i];
        option.text = array[i];
        selectList.appendChild(option);

    }
    return selectList;
}

function getLanguagesList() {

    let arrayLanguage = externalService.getLanguagesList();
    console.log(arrayLanguage);
    for (let i = 0; i < arrayLanguage.length; i++) {
        let options = document.createElement("option");
        options.value = arrayLanguage[i];
        options.text = arrayLanguage[i];
        selectList.appendChild(options);
    }
    return selectList;
}


function radioInputOnChange(e) {
    let target = e.target;
    switch (target.id) {
        case 'by_region':
            removeOptions();
            getRegionsList();
            break;
        case 'by_language':
            removeOptions();
            getLanguagesList();
            break;
    }
}


function createRadioInput(id, name) {
    let radioInput = document.createElement('input');
    radioInput.type = "radio";
    radioInput.checked = false;
    radioInput.value = id;
    radioInput.id = id;
    let labelValue = document.createElement('label');
    labelValue.innerHTML = name;
    radioInput.name = "radio_input";
    radioInput.addEventListener('change', radioInputOnChange);
    document.body.appendChild(radioInput);
    document.body.appendChild(labelValue);
}

createRadioInput("by_region", "By Region");
createRadioInput("by_language", "By Language");


// Create text
document.body.insertAdjacentHTML('beforebegin', 'Please choose the type of search:');

document.body.insertAdjacentHTML("afterend", 'Please choose search query');



function getCountryListByRegion() {
    let arrayCountries = externalService.getCountryListByRegion("Europe");
    for (let i = 0; i < arrayCountries.length; i++) {
        console.log(arrayCountries[i]);
    }
    return arrayCountries;
}
// console.log(getCountryListByRegion());


selectList.addEventListener('change', function() {
    let regions = externalService.getRegionsList();
    let regionName = regions[selectList.selectedIndex];
    let countries = externalService.getCountryListByRegion(regionName);

    refreshTable(countries);

})





function refreshTable(objectArray) {
    const fields = ['name', 'area', 'languages', 'region', 'capital', 'flagURL'];
    const fieldTitles = ['Name', 'Area', 'languages', 'Region', 'Capital', 'Flag'];

    const oldTable = document.getElementById('content_table');
    if (oldTable) {
        oldTable.remove();
    }

    let body = document.getElementsByTagName('body')[0];
    let tbl = document.createElement('table');
    let thead = document.createElement('thead');
    let thr = document.createElement('tr');
    fieldTitles.forEach((fieldTitle) => {
        let th = document.createElement('th');
        th.appendChild(document.createTextNode(fieldTitle));
        thr.appendChild(th);
    });
    thead.appendChild(thr);

    tbl.id = 'content_table';
    tbl.appendChild(thead);
    tbl.style.border = '1px solid black';

    let tbdy = document.createElement('tbody');
    objectArray.forEach((object) => {
        let tr = document.createElement('tr');
        fields.forEach((field) => {
            var td = document.createElement('td');
            let cellValue = object[field];
            if (typeof cellValue === 'object') {
                let text = '';
                for (key in cellValue) {
                    text += cellValue[key] + '\n';
                }
                cellValue = text;
            }
            td.appendChild(document.createTextNode(cellValue));

            td.style.border = '1px solid black';
            tr.appendChild(td);
        });
        tbdy.appendChild(tr);
    });
    tbl.appendChild(tbdy);
    body.appendChild(tbl);
    return tbl;
}

let tableData = [
    { name: 'Banana', price: '3.04' },
    { name: 'Orange', price: '2.56' },
    { name: 'Apple', price: '1.45' }
];

refreshTable(tableData);