function reverseNumber(num) {
    var result = new String();
    if (num < 0) {
        num = Math.abs(num);
        result += '-';
    }
    for (var i = 0; i < num.toString().length; i++) {
        var lastNumber = num / Math.pow(10, i + 1);
        var numbers = new String(lastNumber);
        result += numbers.split('.')[1][0];
    }
    return result;
}
console.log(reverseNumber(12345));
console.log(reverseNumber(-56789));

function forEach(arr, func) {
    for (var i = 0; i < arr.length; i++) {
        func(arr[i]);
    }
}
var arr = [2, 5, 8];
var func = function(el) {
    console.log(el + 1)
};
forEach(arr, func);
forEach([2, 5, 8, 7, 0, 6], function(el) {
        console.log(el)
    }) // logs to console: 2 5 8

function map(arr, func) {
    var array = [];
    forEach(arr, function(el) {
        array.push(func(el));
    });
    return array;
}
console.log(map([2, 5, 8], function(el) {
    return el + 3;
}));
map([2, 5, 8], function(el) {
        return el + 3;
    }) // returns [5, 8, 11]
map([1, 2, 3, 4, 5], function(el) {
        return el * 2;
    }) // returns [2, 4, 6, 8, 10]

function filter(arr, func) {
    var array = [];
    forEach(arr, function(element) {
        if (func(element)) {
            array.push(element)
        };
    });
    return array;
}
console.log(filter([1, 4, 6, 7, 8, 10], function(el) {
    return el % 2 === 0
}))
filter([2, 5, 1, 3, 8, 6], function(el) {
        return el > 3
    }) // returns [5, 8, 6]
filter([1, 4, 6, 7, 8, 10], function(el) {
        return el % 2 === 0
    }) //returns [4, 6, 8, 10]

function getAdultAppleLovers(data) {
    var filteredData = filter(data, function(el) {
        return el.age > 18 && el.favoriteFruit === 'apple'
    });
    return map(filteredData, function(x) {
        return x.name
    });
}
var data = [{
        "_id": "5b5e3168c6bf40f2c1235cd6",
        "index": 0,
        "age": 39,
        "eyeColor": "green",
        "name": "Stein",
        "favoriteFruit": "apple"
    }, {
        "_id": "5b5e3168e328c0d72e4f27d8",
        "index": 1,
        "age": 38,
        "eyeColor": "blue",
        "name": "Cortez",
        "favoriteFruit": "strawberry"
    },
    {
        "_id": "5b5e3168cc79132b631c666a",
        "index": 2,
        "age": 2,
        "eyeColor": "blue",
        "name": "Suzette",
        "favoriteFruit": "apple"
    }, {
        "_id": "5b5e31682093adcc6cd0dde5",
        "index": 3,
        "age": 17,
        "eyeColor": "green",
        "name": "Weiss",
        "favoriteFruit": "banana"
    }
]

console.log(getAdultAppleLovers(data));

function getKeys(obj) {
    var arr = [];
    for (const property in obj) {
        arr.push(`${property}`);
    }
    return arr;
}
console.log(getKeys({ keyOne: 1, keyTwo: 2, keyThree: 3 })); // returns [“keyOne”, “keyTwo”, “keyThree”]

function getValues(obj) {
    var arr = [];
    for (const property in obj) {
        arr.push(`${obj[property]}`);
    }
    return arr;
}
console.log(getValues({ keyOne: 1, keyTwo: 2, keyThree: 3 })) // returns [1, 2, 3]

function showFormattedDate(dateObj) {
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];
    var result = `It is ${dateObj.getDate()} of ${monthNames[dateObj.getMonth()]}, ${dateObj.getFullYear()}`;
    return result;
}
console.log(showFormattedDate(new Date('2018-08-27T01:10:00'))); // returns ‘It is 27 of Aug, 2018’