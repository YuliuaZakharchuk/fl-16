// Your code goes here

function isEquals(num1, num2) {
    return num1 === num2;
}
// console.log(isEquals(3, 4));

function isBigger(num1, num2) {
    return num1 > num2;
}
// console.log(isBigger(-1, 5));

function storeNames() {
    return arguments;
}

// console.log(storeNames('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy', 'Tom Hardy'));
// => ['Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy']

function getDifference(one, two) {
    if (one > two) {
        return one - two;
    } else {
        return two - one;
    }
}
// console.log(getDifference(5, 8));
// getDifference(5, 3) // => 2
// getDifference(5, 8) // => 3


function negativeCount(arr) {
    let arrayNumber = 0;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] < 0) {
            arrayNumber += 1;
        }
    }
    return arrayNumber;
}
// console.log(negativeCount([4, -3, -2, 9]));
// negativeCount([4, 3, 2, 9]) // => 0 
// negativeCount([0, -3, 5, 7]) // => 1

function letterCount(word, letter) {
    let count = 0;
    for (let i = 0; i < word.length; i++) {
        if (word[i] === letter) {
            count += 1;
        }
    }
    return count;
}
// console.log(letterCount("", "z"));
// letterCount("Marry", "r") // => 2 
// letterCount("Barny", "y") // => 1 
// letterCount("", "z") // => 0

const WINNING_POINTS = 3;

function countPoints(arr) {
    let points = 0;
    for (let i = 0; i < arr.length; i++) {
        let element = arr[i];
        let x = parseInt(element.split(':')[0]);
        let y = parseInt(element.split(':')[1]);

        if (x > y) {
            points += WINNING_POINTS;
        } else if (x < y) {
            points;
        } else {
            points += 1;
        }
    }
    return points;
}
// console.log(countPoints(['100:90', '110:98', '100:100', '95:46', '54:90', '99:44', '90:90', '111:100'])); // => 17