/* START TASK 1: Your code goes here */
let table = document.getElementById('table');
if (table !== null) {
    for (let i = 0; i < table.rows.length; i++) {
        for (let j = 0; j < table.rows[i].cells.length; j++) {
            table.rows[i].cells[j].onclick = function() {
                tableColor(this.id);
                changeRowColor(this.id);
                if (this.id === 'column_32') {
                    specialCellChange();
                }
            };
        }
    }
}

function tableColor(cellId) {
    let cell = document.getElementById(cellId);
    cell.style.backgroundColor = 'yellow';
}

function changeRowColor(cellId) {
    let cell = document.getElementById(cellId);
    let tr = cell.parentElement;
    if (cell === tr.cells[0]) {
        for (let i = 0; i < tr.cells.length; i++) {
            tr.cells[i].style.backgroundColor = 'blue';
        }
    }
}

function specialCellChange() {
    for (let i = 0; i < table.rows.length; i++) {
        for (let j = 0; j < table.rows[i].cells.length; j++) {
            console.log(table.rows[i].cells[j].style.backgroundColor);
            if (table.rows[i].cells[j].style.backgroundColor !== 'blue') {

                table.rows[i].cells[j].style.backgroundColor = 'yellow';
            }
        }
    }
}


/* END TASK 1 */

/* START TASK 2: Your code goes here */
let inputValid = document.getElementById('input'),
    btn = document.getElementById('btn'),
    isValidForm = document.getElementById('isValidForm');
inputValid.addEventListener('input', function() {
    let reg = /^\+380\d{9}/g;
    let p = document.createElement('p');
    p.style.backgroundColor = 'red';

    if (!reg.test(inputValid.value)) {
        p.innerText = 'Type phone number does not follow format +380**********';
        p.classList = 'invalid';
        isValidForm.innerHTML = '';
        isValidForm.appendChild(p);
        btn.setAttribute('disabled', true);
        btn.addEventListener('submit', function() {
            p.innerText = 'Data was successfully sent';
        })
    } else {
        p.innerText = 'Data was successfully sent';
        p.classList = 'valid';
        p.style.backgroundColor = 'green';
        isValidForm.innerHTML = '';
        isValidForm.appendChild(p);
        inputValid.style.border = '1px solid green';
        btn.removeAttribute('disabled');

    }
})


/* END TASK 2 */

/* START TASK 3: Your code goes here */
let court = document.getElementById('court');
let ball = document.getElementById('ball');
let width = 600;
let height = 330;
let k = 2;
let halfBall = 20;
let centerX = width / k;
let centerY = height / k;
court.style.width = `${width}px`;
court.style.height = `${height}px`;
ball.style.width = '40px';
ball.style.height = '40px';
ball.style.left = `${centerX- halfBall}px`;
ball.style.top = `${centerY - halfBall}px`;
court.addEventListener('click', function(event) {
    ball.style.left = `${event.clientX-halfBall}px`;
    ball.style.top = `${event.clientY-halfBall}px`;
})

/* END TASK 3 */