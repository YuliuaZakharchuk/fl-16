// Your code goes here
function loadingPage() {
    var loadingPage = prompt('Inputs event name: ', 'metting');
    if (loadingPage) {
        document.getElementById('form').style.visibility = 'visible';
    }
}
loadingPage();

var confirmButton = document.getElementById('confirm');
confirmButton.onclick = function checkForm() {
    var name = document.getElementById('inputName').value;
    var time = document.getElementById('inputTime').value;
    var place = document.getElementById('inputPlace').value;
    if (!name.length || !place.length || !time.length) {
        alert("Input all data");
        return;
    }
    var isValid = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/.test(time);
    if (!isValid) {
        alert("Enter time in format hh:mm");
        return;
    }
    console.log(`${name} has a meeting today at ${time} somewhere in ${place}`);
    return;
}
var converter = document.getElementById('converter');
converter.onclick = function addCount() {
    var euro = prompt("Inputs amount of euro: ");
    var dollar = prompt("Inputs amount of dollar: ");
    var euroConverRate = 33.60;
    var dollarConvertRate = 27.55;
    if (euro > 0 && dollar > 0) {
        var amountEuro = euro * euroConverRate;
        var amountDollar = dollar * dollarConvertRate;
        alert(`${euro} euros are equal ${amountEuro.toFixed(2)}hrns, 
        ${dollar} dollars are equal ${amountDollar.toFixed(2)}hrns`)
    }
    return;
}