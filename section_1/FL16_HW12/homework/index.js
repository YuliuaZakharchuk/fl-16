// Your code goes here
function getAge(birthday22) {
    let enterDate = birthday22.toString();
    const yearUser = birthday22.getFullYear();
    console.log(yearUser);
    if (enterDate !== null) {
        let now = new Date();
        let year = now.getFullYear();
        return year - yearUser;
    }
}
const year = 2000;
const month = 9;
const day = 22;
const birthday22 = new Date(year, month, day);
getAge(birthday22);

function getWeekDay() {
    let d = new Date();
    let arr = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let m = arr[d.getDay()];
    for (let i = 0; i < arr[i]; i++) {
        console.log(arr[i]);
        m = d.getDay();
    }
    return m;
}
getWeekDay();

function getAmountDaysToNewYear() {
    let today = new Date();
    let milisecond = 86400000;
    let numberDay = Math.ceil((today - new Date(today.getFullYear(), 0, 1)) / milisecond);
    return numberDay;
}
getAmountDaysToNewYear();

function getProgrammersDay(year) {
    let y = 2000;
    let m = 0;
    let date = new Date(y, m);
    let dayStart = 256;
    date.setDate(dayStart);
    let k = 4;
    let l = 100;
    let r = 400;
    if (0 === year % k && 0 !== year % l || 0 === year % r) {
        let yearsNumber = getWeekDay(new Date(y, m));
        console.log(date.getDate(), date.getMonth(), date.getFullYear(), yearsNumber);
    } else {
        console.log(year + ' is not a leap year');

    }
}
const h = 2000;
getProgrammersDay(h);

let weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

function howFarIs(userDayName) {
    let userWeekDay = null;
    let todayWeekDay = new Date().getDay();
    for (let i = 0; i < weekdays.length; i++) {
        if (userDayName === weekdays[i]) {
            userWeekDay = i;
        }
    }
    if (userWeekDay === todayWeekDay) {
        return `Hey, today is ${weekdays[userWeekDay]}`;
    }
    if (userWeekDay > todayWeekDay) {
        return `It's ${userWeekDay-todayWeekDay} day(s) left till ${weekdays[userWeekDay]}.`
    }
    if (userWeekDay < todayWeekDay) {
        let week = 7;
        return `It's ${week-todayWeekDay+userWeekDay} day(s) left till ${weekdays[userWeekDay]}.`
    }
}

howFarIs('Saturday');
howFarIs('Thursday');

function isValidIdentifier(row) {
    let example = /^(?=.*[^\d][a-z][A-Z])(?=.*[{Alpha}])|(?=.*[0-9])|(?=.*['_'])|(?=.*[$])*$/.test(row);
    return example;
}
isValidIdentifier('myVar!');
isValidIdentifier('myVar$');
isValidIdentifier('myVar_1');
isValidIdentifier('1_myVar');

function capitalize(testStr) {
    const mySentence = testStr;
    const finalSentence = mySentence.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());
    return finalSentence;
}

const testStr = 'My name is John Smith. I am 27.';
capitalize(testStr);

function isValidAudioFile(j) {
    let fileName = /^[a-z]+(.aac|.mp3|.flac|.alac)*$/.test(j);
    return fileName;
}

isValidAudioFile('file.mp4');
isValidAudioFile('my_file.mp3');
isValidAudioFile('file.mp3');

function getHexadecimalColors(testString) {
    let hexTagColors = /#([a-f0-9]{3}){1,2}\b/ig;
    let hex = testString.match(hexTagColors)
    return hex;

}

const testString = 'color: #3f3; background-color: #AA00ef; and: #abcd';
getHexadecimalColors(testString);
getHexadecimalColors('red and #0000');

function isValidPassword(h) {
    let password = /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{8,}.*$/.test(h)
    return password;
}

isValidPassword('agent007');
isValidPassword('AGENT007');
isValidPassword('AgentOOO');
isValidPassword('Age_007');
isValidPassword('Agent007');

function addThousandsSeparators(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}
const num = 1234567890;
addThousandsSeparators(num);

function getAllUrlsFromText(text1) {
    let url = /(https:\/\/.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/g;
    return text1.match(url);
}
const text1 = 'We use https://translate.google.com/ to translate some words and phrases from https://angular.io/';
const text2 = 'JavaScript is the best language for beginners!'
getAllUrlsFromText(text1);