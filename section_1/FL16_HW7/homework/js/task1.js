// Your code goes here

var buttonEnter = document.getElementById('button');
buttonEnter.onclick = function() {
    const firstSuma = parseInt(prompt('Input initial amount of money'));
    console.log(typeof firstSuma);
    if (!firstSuma || firstSuma < 1000) {
        alert("Invalid input data");
        return;
    }

    const years = parseInt(prompt('Input years:'));
    if (!years || years < 1) {
        alert("Invalid input data");
        return;
    }
    const interest = parseInt(prompt('Percentage of year:'));
    if (!interest || interest > 100) {
        alert("Invalid input data");
        return;
    }
    searchInterest();

    function searchInterest() {
        var generalProfit = 0;
        var nextSuma = firstSuma;
        for (var i = 0; i < years; i++) {
            var muches = nextSuma * interest / 100;
            nextSuma = nextSuma + muches;
            generalProfit += muches;
        }
        alert(`Initial amount: ${firstSuma} \n Number of years: ${years} \n Percentage of year: ${interest} \n 
        \n Total profit: ${generalProfit.toFixed(2)} \n Total amount: ${nextSuma.toFixed(2)} `);
    }
}