// Your code goes here
const x = 8;
const arr = [100, 50, 25];
var totalPrice = 0;

function genarateRandomNumber(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function gameRound(maxNumber, prices) {
    var attemptsLeft = 3;
    var correctNumber = genarateRandomNumber(0, maxNumber);
    console.log(correctNumber);
    while (attemptsLeft > 0) {
        const chosenNumber = parseInt(prompt(
            'Choose a roullette pocket number from 0 to' + maxNumber + '\n Attemps left: ' +
            attemptsLeft + '\n Total prize: ' + totalPrice + '\n Possible prize on current attemps:' +
            prices[3 - attemptsLeft]
        ));
        if (chosenNumber === correctNumber) {
            totalPrice = totalPrice + prices[3 - attemptsLeft];
            return totalPrice;
        } else {
            attemptsLeft -= 1;
        }
    }
    return false;
}

function startGame() {
    var round = 0;
    var isContinue = true;
    totalPrice = 0;
    while (isContinue) {
        var result = gameRound(
            x + 4 * round, [arr[0] * Math.pow(2, round), arr[1] * Math.pow(2, round), arr[2] * Math.pow(2, round)]
        );
        if (result) {
            alert('Congratulation, you won! Your prize is:' + totalPrice + '$.');
            round += 1;
        } else {
            isContinue = confirm("Thank you for your participation. Your prize is: " +
                totalPrice + "$. Do you want to continue?");
            round = 0;
            totalPrice = 0;
        }
    }
}

var result = confirm("Do you want to play a game?");

if (result) {
    startGame();
} else {
    alert('You did not become a billionaire, but can.');
}